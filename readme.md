# CocktailFinder
##### React Native Challenge
### Install
Open terminal and run:
#### Linux
```sh
npm install
```
#### Mac
```sh
yarn install
cd ios && pod install
```
### Libraries used
* [react-native]
* [typescript]
* [react-navigation]
* [react-navigation-stack]
* [react-native-vector-icons]
* [redux]
* [react-native-linear-gradient]
### Posible optimizations
* Implement pagination (require server side changes).
* Abort fetch instead of just ignore.
* Caching search results.
* Caching images.